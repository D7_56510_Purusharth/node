const express =require('express')
const db = require('../db');
const utils = require('../utils');
const router = express.Router();

router.get('/',(req,resp)=>{
    resp.send('Welcome to Employee Application!!!');
})

router.get('/all',(req,resp)=>{
    
    const statement = `SELECT empid,name FROM Emp`

    const connection = db.openDatabasesConnection();
    connection.query(statement,(error,result) => {
        connection.end();
        resp.send(utils.createResult(error,result));
    })
})

router.post('/add',(req,resp) => {
    const { name , salary , age } = req.body;
    const connection = db.openDatabasesConnection();
    const statement = `INSERT INTO Emp ( name, salary, age)
                        VALUES
                            ('${name}',${salary},${age})`;
    connection.query(statement,(error,result) => {
        connection.end();
        resp.send(utils.createResult(error,result));
    });
});

router.delete('/delete/:id',(req,resp) => {
    const { id } = req.params
    const statement = `DELETE FROM Emp WHERE empid = ${id}`;
    const connection = db.openDatabasesConnection();
    connection.query(statement,(error,result) => {
        connection.end();
        resp.send(utils.createResult(error,result));
    });
});

router.put('/update/:id',(req,resp) => {
    const { id } = req.params;
    const{ salary } = req.body;

    const statement = `UPDATE Emp 
                            SET salary =  ${salary} 
                                WHERE empid = ${id}`;
                            
    const connection = db.openDatabasesConnection();
    connection.query(statement,(error,result) => {
        connection.end();
        resp.send(utils.createResult(error,result));
    })
})

module.exports = router;