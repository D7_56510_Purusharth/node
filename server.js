const express = require('express')
const cors = require('cors')
const empRouter = require('./routes/emp');


const app = express();
app.use(express.json());
app.use(cors('*'));

app.use('/emp',empRouter);

app.listen(4000, "0.0.0.0",() => {
    console.log(`server started at 4000`);
})